define(['aui-mocha'], function() {
    'use strict';
    var fixtureElement;

    beforeEach(function () {
        fixtureElement = document.getElementById('test-fixture');
        if (!fixtureElement) {
            fixtureElement = document.createElement('div');
            fixtureElement.id = 'test-fixture';
            document.body.appendChild(fixtureElement);
        }
    });

    afterEach(function () {
        fixtureElement.innerHTML = '';
        AJS.mocha.warnIfLayersExist();
    });
});
